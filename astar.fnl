(global pp (fn [x] (let  [view (require "lib.fennelview")] (print (view x)))))

;; point.fnl
(local point {})

(lambda point.new [?x ?y]
  (let [p {:x (or ?x 0) :y (or ?y 0)}]
    p))

(fn point.set [p x y]
    (tset p :x x)
    (tset p :y y)
    p)

(fn point.equals [p1 p2]
  (and (=  p1.x p2.x)
       (= p2.y p1.y)))

(fn point.add [p1 p2]
  (point.new (+  p1.x p2.x)
             (+ p2.y p1.y)))

(fn difference [p1 p2]
  [(- p2.x p1.x) (- p2.y p1.y)])


(fn point.distance [p1 p2 callback]
  (let [[dx dy] (difference p1 p2)]
    (callback dx dy)))

(fn point.distance-euclidian [dx dy]  
  (math.sqrt (+ (math.pow dx 2) (math.pow dy 2))))

(fn point.distance-manhattan [dx dy]
  (+ (math.abs dx) (math.abs dy)))

(fn point.distance-chebyshev [dx dy]
  (math.max dx dy))

point

;; node.fnl
;; callbacks used by astar
(local node {})
;; (local point (require "point"))

(lambda node.new [?pos ?parent ?dist ?cost]
  {:pos (or ?pos (point.new))
   :parent (or ?parent (point.new))
   :dist (or ?dist 0)
   :cost(or ?cost 0)})

(fn node.cost-function [node]
  (+ node.dist node.cost))

(fn node.compare [node-a node-b]
  (point.equals node-a.pos node-b.pos))

(fn node.distance [node-a node-b]
  (point.distance node-a.pos node-b.pos point.distance-chebyshev))

(fn node.inherit [old-node edge goal]
  (let [pos (point.add old-node.pos (point.new (. edge 1) (. edge 2)))]
    (node.new pos old-node.pos (node.distance {:pos pos} goal)
              (+ old-node.cost (. edge 3)))))

(fn node.parent-p [n parent]
  (point.equals n.parent parent.pos))

(fn node.returns [n]
  n.pos)

(fn node.has-node [arr n]
  ;; generalize as member (in utils)
  (var ret -1)
  (each [key val (ipairs arr)]
    (when  (node.compare val n)
      (set ret key)))
  ret)

(fn node.comparison-value [n]
  n.pos)

node

;; map.fnl
;; required callbacks by a*
(local map {})
(fn map.index-value [x y width]
  (+ x (* width y )(-  width)))

(fn map.is-valid-p [map pos]
  (and (> pos.x 0)
       (<= pos.x map.width)
       (> pos.y 0)
       (<= pos.y map.height)
       (= (. map.values  (map.index-value pos.x pos.y map.width)) 0)))

map

(local astar {})
;; (local map (require "map"))
;; (local node (require "node"))

(fn table-index-swap [t i j]
  (let [s (. t i)]
    (tset t i (. t j))
    (tset  t j s))
  t)

(fn get-smallest [q callback]
    (each [i value (ipairs q)]
      (when (and (< i 1)
                 (~= (. q i) nil)
                 (~= (. q 1) nil)
                 (< (callback (. q i)) (callback (. q 1))))
        (table-index-swap q 1 i)))
    (table.remove q 1))

(fn add-to-open [current-node map node goal open closed]
  (var done false)
  (each [_ edge (ipairs (or current-node.nbours map.nbours))]
    (let [new-node (node.inherit current-node edge goal)]
      (when (map.is-valid-p map (node.comparison-value new-node))        
        (if (node.compare goal new-node)
            (do (table.insert closed new-node)
                (set done true))                
            (and (not done)                                                     
                   (< (node.has-node closed new-node) 0))
            (let [index (node.has-node open new-node)]
              (if (or (< index 0 )
                      (and (> index 0)
                           (< (node.cost-function new-node)
                              (node.cost-function (. open index)) )))
                  (do (when (> index 0)
                        (table.remove open index))                      
                      (table.insert open new-node)))
              )))))
  done)

(fn make-path [closed map node]
  (local length (# closed))
  (var current-node (. closed length))  
  (var l [(node.returns current-node)])
  (for [i (- length 1) 1 -1]
    (let [parent (. closed i)]
      (when (and (~= current-node nil) (node.parent-p current-node parent))
        (table.insert l (node.returns parent))
        (set current-node parent))))
  l)

(fn astar.astar [map node start goal]
  (var valid 0)
  (var open {})
  (var closed {})
  (table.insert open start)
  (while (= valid 0)
    (let [current-node (get-smallest open node.cost-function)]
      (if (not (= nil current-node))
          (do
            (table.insert closed current-node)                          
            (when (= true (add-to-open current-node map node goal open closed))
              (set valid 1)))
          (set valid -1))))
  (if (= valid 1)
      [true (make-path closed map node)]
      [false "no path"]))

astar

;; utils.fnl
(local utils {})
(lambda utils.member [arr n callback]
  (var ret -1)
  (each [key val (ipairs arr)]
    (when  (callback val n)
      (set ret key)))
  ret)

utils

;; user space
;; (local map (require "map"))
;; (local astar (require "astar"))
;; (local node (require "node"))
;; (local utils (require "utils"))

(fn path-compare [b a]
  (let [[x1 y1] a [x2 y2] [b.x b.y]]
    (and (= x1 x2) (= y1 y2))))

(fn astar-test-draw [map path tiles]
  (for [j 1 map.height]
    (for [i 1 map.width]
      (let [m (. map.values (map.index-value i j map.width))]
        (if (and (= m 0) (< (utils.member path [i j] path-compare) 0))
            (io.write (.. (. tiles 1) " "))
            (= m 1) (io.write (.. (. tiles 2)" "))
            (io.write (.. (. tiles 3) " ")))))
    (io.write "\n")))

(fn astar-test [start-pos end-pos tiles]
  (local nbours [[ 1  0 1  ][ 0 1 1  ][1  1 1.4][ 1 -1 1.4]
                 [-1 -1 1.4][-1 1 1.4][0 -1 1  ][-1  0 1  ]])
  (local goal (node.new (point.new (. end-pos 1) (. end-pos 2))))
  (local start (let [point (point.new (. start-pos 1) (. start-pos 2))]
                 (node.new point nil (node.distance goal {:pos point}))))
  (local map-values
         [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
          1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1 
          1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
          1 0 0 0 0 1 1 1 0 1 0 0 0 0 0 0 0 0 0 1
          1 0 0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 1
          1 0 0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 1
          1 0 0 1 1 1 1 1 0 1 0 0 0 0 0 0 0 0 0 1
          1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1
          1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1
          1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1])  
  (var my-map {:width 20 :height 10
               :values map-values
               :nbours nbours
               })
  (set my-map.index-value map.index-value)
  (set my-map.is-valid-p map.is-valid-p)
  (let [[done path](astar.astar my-map node start goal)]
    (if done
        (do
          (print "Path found!")
          (astar-test-draw my-map path tiles)
          path)
        (do (print "could not find path!")
            (astar-test-draw my-map [start] tiles)))))

(global atest astar-test)

(pp (atest [2 9] [13 9] ["0" "1" "2"]))
